from classes.photoboothPi import PhotoboothPi
options = {
	'button' : 'images/screen_button_new.gif',
	'waiting' : 'images/preview_instruction.gif',
	'saving' : 'images/how_you_looked.gif',
	'countdown_three' : 'images/taking_photo_3.gif',
	'countdown_two' : 'images/taking_photo_2.gif',
	'countdown_one' : 'images/taking_photo_1.gif',
	'thank_you' : 'images/thank_you.gif',
	'review_seconds' : 5
}
PhotoboothPi(options);
