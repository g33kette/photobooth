# README #

Python Photobooth

### Dependencies: ###

 * Python 3
 * Pillow (PIL)
 
#### Installing Pillow ####

```
#!bash

$ sudo apt-get install python3-pip
$ sudo apt-get install libjpeg-dev
$ sudo apt-get install tk8.5-dev
$ sudo pip-3.2 install Pillow

```

### Using Class: ###

```
#!python

from classes.photoboothPi import PhotoboothPi
options = {}
PhotoboothPi(options);

```

#### Options ####

All options are optional and will take default value if omitted.

 * **button** - "Push The Button" image, must be GIF!
 * **waiting** - "Waiting" image, must be GIF!
 * **saving** - "Saving" image, must be GIF!
 * **reverse_button** - Set to true if button is wired in reverse
 * **preview_seconds** - How long in seconds to show preview before taking photo (default = 5)
 * **review_seconds** - How long in seconds to review photo afterwards (default = 3)

### How to run: ###


```
#!bash

$ cd /path/to/script
$ sudo python3 photobooth.py
```

### Options: ###

 * **--vflip=true** - Virtical flip camera
 * **--hflip=true** - Horizontal flip camera
 * **--brightness=123** - Adjust brightness by 123
 * **--prefix=wedding** - Prefix all photo names with "wedding" (Defaults to "Photobooth)
 * **--parent_folder='./wedding_photos** - Folder to save photos (Defaults to "./photos") 
