import RPi.GPIO as GPIO
import time
import picamera
import sys
import getopt
import os
from .photoboothInterface import PhotoboothInterface

class PhotoboothPi:

	button_pressed = False
	review_seconds = 3
	preview_seconds = 5

	camera_brightness = False
	camera_vflip = False
	camera_hflip = False
	parent_folder = './photos'
	image_prefix = 'Photobooth'

	def __init__(self, options):
		print('Photobooth starting up...')
		if 'reverse_button' in options and options['reverse_button'] == True:
			self.button_pressed = True
		if 'review_seconds' in options:
			self.review_seconds = options['review_seconds']
		if 'preview_seconds' in options:
			self.preview_seconds = options['preview_seconds']
		self.interface = PhotoboothInterface(options)
		self.parseOptions()
		self.listenForButton()
		
	def parseOptions(self):
		print('Parsing options...')
		for arg in sys.argv:
			option = arg.split('=',1)
			if (option[0] == '--vflip' and option[1] == 'true'):
				self.camera_vflip = True
			elif (option[0] == '--hflip' and option[1] == 'true'):
				self.camera_hflip = True
			elif (option[0] == '--brightness'):
				self.camera_brightness = option[1]
			elif (option[0] == '--prefix'):
				self.image_prefix = option[1]
			elif (option[0] == '--folder' and os.path.exists(option[1])):
				self.parent_folder = option[1]
				if (self.parent_folder[-1:] == '/'):
					self.parent_folder = self.parent_folder[:-1]				
		if (self.parent_folder[:2] == './'):
			self.parent_folder = os.getcwd() + '/' + self.parent_folder[2:]
		return

	def listenForButton(self):
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		self.interface.showButton()
		print('Ready, waiting for button press!')
		while True:
			input_state = GPIO.input(18)
			if input_state == self.button_pressed:
				print('Button Pressed')
				self.takePhoto()
				self.interface.showThankYou()
				time.sleep(1)
				self.interface.showButton()
				print('Photo taken, waiting for another button press!')
				time.sleep(0.2)
		return
		
	def takePhoto(self):
		self.interface.showWaiting()
		time.sleep(2)
		if not os.path.exists(self.parent_folder):
			os.makedirs(self.parent_folder)
		folder = self.parent_folder + '/' + time.strftime('%Y %B %d')
		if not os.path.exists(folder):
			os.makedirs(folder)
		with picamera.PiCamera() as camera:
			camera.vflip = self.camera_vflip
			camera.hflip = self.camera_hflip
			if self.camera_brightness:
				camera.brightness = self.camera_brightness
			camera.start_preview()
			path = folder + '/' + self.image_prefix + ' ' + str(int(time.time())) + '.jpg'
			#print(path)
			time.sleep(self.preview_seconds)
			self.interface.showCountdownThree()
			camera.stop_preview()
			time.sleep(1)
			self.interface.showCountdownTwo()
			time.sleep(0.2)
			self.interface.showCountdownOne()
			time.sleep(0.2)
			camera.capture(path)
			self.interface.showSaving()
			time.sleep(1)
			self.interface.showPreview(path)
			time.sleep(self.review_seconds)
		return

	

