from tkinter import *
from PIL import Image
from PIL import ImageTk

class PhotoboothInterface:

	button = 'button.gif'
	waiting = 'waiting.gif'
	countdownOne = 'countdownOne.gif'
	countdownTwo = 'countdownTwo.gif'
	countdownThree = 'countdownThree.gif'
	saving = None
	
	def __init__(self, options):
		if 'button' in options:
			self.button = options['button']
		if 'waiting' in options:
			self.waiting = options['waiting']
		if 'countdown_one' in options:
			self.countdown_one = options['countdown_one']
		if 'countdown_two' in options:
			self.countdown_two = options['countdown_two']
		if 'countdown_three' in options:
			self.countdown_three = options['countdown_three']
		if 'thank_you' in options:
			self.thank_you = options['thank_you']
		if 'saving' in options:
			self.saving = options['saving']
		else:
			self.saving = self.waiting
		self.tk = Tk()
		self.tk.attributes('-zoomed', True)
		self.tk.attributes('-fullscreen', True)
		self.width = self.tk.winfo_screenwidth()
		self.height = self.tk.winfo_screenheight()
		self.canvas = Canvas(self.tk, width=self.width, height=self.height)
		self.prepareButton()

	def startCallback(self, event=None):
		self.tk.quit()
		return

	def prepareButton(self):		
		button=PhotoImage(file=self.button)
		self.canvas.button=button		
		return

	def showButton(self):
		self.showImage(self.canvas.button)
		return

	def preparePreview(self, path):
		image = Image.open(path)
		image = self.fillScreen(image)
		preview = ImageTk.PhotoImage(image)
		self.canvas.preview=preview
		return

	def showPreview(self, path):
		self.preparePreview(path)
		self.showImage(self.canvas.preview)
		return
		
	def prepareWaiting(self):		
		waiting=PhotoImage(file=self.waiting)
		self.canvas.waiting=waiting		
		return

	def showWaiting(self):
		self.prepareWaiting()
		self.showImage(self.canvas.waiting)
		return

	def prepareCountdownOne(self):		
		countdown_one=PhotoImage(file=self.countdown_one)
		self.canvas.countdown_one=countdown_one		
		return

	def showCountdownOne(self):
		self.prepareCountdownOne()
		self.showImage(self.canvas.countdown_one)
		return

	def prepareCountdownTwo(self):		
		countdown_two=PhotoImage(file=self.countdown_two)
		self.canvas.countdown_two=countdown_two		
		return

	def showCountdownTwo(self):
		self.prepareCountdownTwo()
		self.showImage(self.canvas.countdown_two)
		return

	def prepareCountdownThree(self):		
		countdown_three=PhotoImage(file=self.countdown_three)
		self.canvas.countdown_three=countdown_three		
		return

	def showCountdownThree(self):
		self.prepareCountdownThree()
		self.showImage(self.canvas.countdown_three)
		return
	
	def prepareSaving(self):		
		saving=PhotoImage(file=self.saving)
		self.canvas.saving=saving		
		return

	def showSaving(self):
		self.prepareSaving()
		self.showImage(self.canvas.saving)
		return

	def prepareThankYou(self):		
		thank_you=PhotoImage(file=self.thank_you)
		self.canvas.thank_you=thank_you		
		return

	def showThankYou(self):
		self.prepareThankYou()
		self.showImage(self.canvas.thank_you)
		return

	def showImage(self, image):
		self.canvas.delete('all');
		self.canvas.create_image((self.width/2)-2, (self.height/2)-2, image=image)
		self.canvas.pack()
		self.tk.after(1000, self.startCallback)
		self.tk.mainloop()
		return
		
	def fillScreen(self, image):
		image = image.resize((self.width+2, self.height+2))
		return image
